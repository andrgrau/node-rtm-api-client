# node-rtm-api-client

A node.js library for accessing the Remember The Milk (RTM) API. The API is "designed" to be a fluent API.

## Exmaple usage
```
var rtm = new Rtm('apiKey', 'sharedSecret');

// You need the user to authenticate you app by using the authentication URL and afterwards you'll get a frob ...
console.log( rtm.createAuthUrl() );

// ... which is needed to get an auth token
rtm.callMethod('rtm.auth.getToken').
    withParam('frob', frob).
    nowForTokenAndCall(null, function callRtmGetToken(result, err) {
        var resultJson = JSON.parse(result);
        var token = resultJson.rsp.auth.token;
        // Save this token, you need it every time for this particular user!
    };

// You should most probably call for a timeline which is often used by RTM.
rtm.callMethod('rtm.timelines.create').
    nowForTokenAndCall(token, function callRtmGetTimeline(result, err) {
        var resultJson = JSON.parse(result);
        var timeline = resultJson.rsp.timeline;
        // Save your timeline for later
    };

// Get all lists of a user (identified by token)
rtm.callMethod('rtm.lists.getList').
    nowForTokenAndCall(token, function callRtmListsGetlist(result, err) {
        var resultJson = JSON.parse(result);
        var arrayOfLists = resultJson.rsp.lists.list;
        // Do with your lists whatever you want :-)
    };

// Get tasks of a specific list if a user (identified by token)
rtm.callMethod('rtm.tasks.getList').
    withParam('list_id', listId).
    nowForTokenAndCall(token, function callRtmTasksGetlists(result, err) {
        var resultJson = JSON.parse(result);
        var arrayOfTaskseries = resultJson.rsp.tasks.list.taskseries;
        // Do with the tasks / taskseries whatever you want :-)
    };

```