var https = require('https');
var crypto = require('crypto');

// a good read on:
// timelines https://www.rememberthemilk.com/services/api/timelines.rtm
// taskseries https://www.rememberthemilk.com/services/api/tasks.rtm

/**
 * Create an instance of {Rtm}. Afterwards you probably want to call {Rtm.callMethod} for every user before you access
 * RTM for him!
 * @param apiKey {String} API key to be used
 * @param sharedSecret {String} shared secret to be used
 * @constructor
 */
var Rtm = function (apiKey, sharedSecret) {
    /** @private */
    this._apiKey = apiKey;
    /** @private */
    this._sharedSecret = sharedSecret;

    /**
     * Set default values where necessary
     */
    /** @private */
    this._rtmMethod = 'rtm.test.echo';
    /** @private */
    this._params = {};
};

/**
 * Method will create the authentication URL for RTM. The user has to be redirected to this URL where he can
 * authenticate himself so that we are legitimated to read, write and delete his data.
 * @return {String} authentication URL
 */
Rtm.prototype.createAuthUrl = function () {
    this._params = {};
    this.withParam('api_key', this._apiKey);
    this.withParam('perms', 'delete');

    return 'http://www.rememberthemilk.com/services/auth/' + this._convertParameters();
};

/**
 * Sets the method which is later called by {Rtm.nowForTokenAndCall}. Parameters needed by the method must be added
 * AFTERWARDS by calling {Rtm.withParam} because {Rtm.callMethod} will clean all previously set parameters.
 * @param rtmMethod {String} RTM method, e.g. 'rtm.test.echo'
 * @return {Object} this
 */
Rtm.prototype.callMethod = function (rtmMethod) {
    this._rtmMethod = rtmMethod;

    this._params = this._createDefaultParams();

    return this;
};

/**
 * Create the default parameters as {Object} consisting of a minimal set: response format, API key and method
 * @return {Object}
 * @private
 */
Rtm.prototype._createDefaultParams = function () {
    var defaultParams = {};
    defaultParams.format = 'json';
    defaultParams.api_key = this._apiKey;
    defaultParams.method = this._rtmMethod;

    return defaultParams;
};

/**
 * Adds one parameter as key value pair needed by the method to be called. Method must be set by calling
 * {Rtm.callMethod} before adding any parameters.
 * @param key {String} key of the parameter needed by the method to be called
 * @param value {String} value of the parameter needed by the method to be called
 * @return {Object} this
 */
Rtm.prototype.withParam = function (key, value) {
    this._params[key] = value;

    return this;
};

/**
 * The actual API logic: Make a call to the RTM API using previously set parameters. The token for the current user has
 * to be provided, the module is NOT responsible for managing sessions.
 * @param token {String} token returned by RTM after the user authenticated us with a call to the URL returned by
 * {Rtm.createAuthUrl}.
 * @param callback {function(result, error)} The callback function, taking two arguments:
 * 1. Results {Object}
 * 2. {Error} if an error occurred
 */
Rtm.prototype.nowForTokenAndCall = function (token, callback) {
    var that = this;
    this.withParam('auth_token', token);
    var optionsGet = this._createGetOptions();

    var reqGet = https.request(optionsGet, function onRequest(response) {
        response.setEncoding('utf8');

        var rtmResponse = '';

        response.on('data', function onData(chunk) {
            rtmResponse += chunk.toString('utf8');
        });

        response.on('end', function onEnd() {
            if (that._didRtmReportAnError(rtmResponse)) {
                callback(rtmResponse, new Error('RTM error!'));
            } else {
                callback(rtmResponse, null);
            }
        });
    });

    reqGet.on('error', function onError(error) {
        console.error(error);
        // TODO Can we call the callback as well here? Unsure if the 'end' event is always emitted.
    });

    reqGet.end();
};

/**
 * Convenience method for manipulations in RTM via the API. Most manipulations in
 * - contact 
 * - group 
 * - list
 * - task
 * do require a timeline so that they can be revoked easily. The method will do the same as {Rtm.nowForTokenAndCall}
 * except that it also submits a timeline id to the API.
 * @param token {String} token returned by RTM after the user authenticated us with a call to the URL returned by
 * {Rtm.createAuthUrl}.
 * @param timeline {String} timeline ID returned by RTM after a call to 'rtm.timelines.create'
 * @param callback {function(error, result)} The callback function, taking two arguments:
 * 1. {Error} if an error occurred
 * 2. Results {Object}
 */
Rtm.prototype.nowForTokenAndTimelineAndCall = function (token, timeline, callback) {
    this.withParam('timeline', timeline);

    this.nowForTokenAndCall(token, callback);
};

/**
 * Create GET options for HTTPS request to RTM and return them as {Object}.
 * @return {Object} options as needed by the http(s) module
 * @private
 */
Rtm.prototype._createGetOptions = function () {
    var paramsAsString = this._convertParameters();

    return {
        host : 'api.rememberthemilk.com',
        path : '/services/rest' + paramsAsString
        /* 
            port defaults to 443,
            method defaults to 'GET'
        */
    };
};

/**
 * Convert parameters to a {String} for the GET parameters. Parameters were previously added step by step after calling
 * {Rtm.withParam} possibly multiple times. The API signature is added as well as we assume that this method is invoked
 * just before the final call to the RTM API.
 * @return {String} Concatenation of parameters as '?key=value&key=value&api_sig=xyz'
 * @private
 */
Rtm.prototype._convertParameters = function () {
    var paramsString = '?';
    var paramKeys = this._sortAndGetParamKeys();

    for (var paramKeyIndex in paramKeys) {
        var paramKey = paramKeys[paramKeyIndex];
        paramsString += paramKey + '=' + this._params[paramKey] + '&';
    }

    // After last character which is a '&' (or '?' if no params are set (which 
    // should not happen at all)) add the api_sig for this request
    paramsString += 'api_sig=' + this._createAndGetApiSig(paramsString);

    return paramsString;
};

/**
 * Get, sort and return all keys of the parameters.
 * @return {Array} parameter keys
 * @private
 */
Rtm.prototype._sortAndGetParamKeys = function() {
    var paramKeys = Object.keys(this._params);
    paramKeys.sort();
    
    return paramKeys;
};

/**
 * Create API signature as requested by RTM. See https://www.rememberthemilk.com/services/api/authentication.rtm for
 * more information.
 * @param paramsString {String} parameters exactly how they are used in the API call
 * @return {String} API signature as MD5
 * @private
 */
Rtm.prototype._createAndGetApiSig = function(paramsString) {
    // remove all occurrences of the URL params special chars ?, = and &
    var paramsAsOneStringWithoutSpecialChars = paramsString.replace(/[?=&]/g, '');
    // prepend the shared secret as required by the RTM API
    var paramsWithSharedSecretToBeHashed = this._sharedSecret + paramsAsOneStringWithoutSpecialChars;
    // hash this final string as required by the RTM API
    var md5 = crypto.createHash('md5');
    //noinspection JSUnresolvedFunction
    return md5.update(paramsWithSharedSecretToBeHashed).digest('hex');
};


/**
 * Validates a response from RTM whether an error occurred.
 * @param {String} rtmResponse RTM's response
 * @return {boolean} true if an error returned by RTM otherwise false
 * @private
 */
Rtm.prototype._didRtmReportAnError = function(rtmResponse) {
    var rtmStat;

    var rtmResponseJson = JSON.parse(rtmResponse);
    //noinspection JSUnresolvedVariable
    rtmStat = rtmResponseJson.rsp.stat;

    if (rtmStat === 'fail') {
        console.error('RTM reported a fail: ' + rtmResponse);
        return true;
    } else {
        return false;
    }
};


module.exports = Rtm;
